//
//  AppDelegate.swift
//  RestKitExample
//
//  Created by Gibran Tavares on 20/07/15.
//  Copyright (c) 2015 Gibran Tavares. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var manager : RKObjectManager = RKObjectManager()
    var managerObjectStore : RKManagedObjectStore = RKManagedObjectStore()
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        self.initRestkit()
        
        return true
    }
    
    func initRestkit() {
        let baseURL = "http://services-dev.unifor.br/services-dev/"
        
        var managedObjectModel = NSManagedObjectModel.mergedModelFromBundles(nil)
        managerObjectStore = RKManagedObjectStore(managedObjectModel: managedObjectModel)
        managerObjectStore.createPersistentStoreCoordinator()
        
        var storePath = RKApplicationDataDirectory().stringByAppendingPathComponent("UniforOnline.sqlite")
        println("database carregado : \(storePath)")
        
        var persistentStore = managerObjectStore.addSQLitePersistentStoreAtPath(storePath, fromSeedDatabaseAtPath: nil, withConfiguration: nil, options: [NSMigratePersistentStoresAutomaticallyOption:true, NSInferMappingModelAutomaticallyOption:true], error: nil)
        
        managerObjectStore.createManagedObjectContexts()
        managerObjectStore.managedObjectCache = RKInMemoryManagedObjectCache(managedObjectContext: managerObjectStore.persistentStoreManagedObjectContext)
        
        manager = RKObjectManager(baseURL: NSURL(string: baseURL))
        RKObjectManager.setSharedManager(manager)
        manager.managedObjectStore = managerObjectStore
        
        RKManagedObjectStore.setDefaultStore(manager.managedObjectStore)
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }


}

