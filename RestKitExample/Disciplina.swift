//
//  Disciplina.swift
//  
//
//  Created by Gibran Tavares on 28/07/15.
//
//

import Foundation
import CoreData

@objc(Disciplina)
class Disciplina: NSManagedObject {

    @NSManaged var codigo: String
    @NSManaged var creditoPratico: NSNumber
    @NSManaged var creditoTeorico: NSNumber
    @NSManaged var ead: NSNumber
    @NSManaged var faltas: NSNumber
    @NSManaged var frequencia: NSNumber
    @NSManaged var horario: String
    @NSManaged var identificador: String
    @NSManaged var multiplosProfessores: NSNumber
    @NSManaged var nf: NSNumber
    @NSManaged var nome: String
    @NSManaged var np1: NSNumber
    @NSManaged var np2: NSNumber
    @NSManaged var sala: String
    @NSManaged var turma: NSNumber
    @NSManaged var curso: Curso
    @NSManaged var professor: Professor

}
