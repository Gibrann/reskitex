//
//  FeedViewController.swift
//  RestKitExample
//
//  Created by Gibran Tavares on 22/07/15.
//  Copyright (c) 2015 Gibran Tavares. All rights reserved.
//

import UIKit
class FeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
 
    var disciplinas : [Disciplina] = []
    var token : String?
    var managedContext: NSManagedObjectContext!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        println(self.disciplinas.isEmpty)
        
        self.initMapping()

    }
    
    
    override func viewWillAppear(animated: Bool) {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(defaults.valueForKey("token")!, forKey: "token")
        
        var delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.managedContext = delegate.managerObjectStore.mainQueueManagedObjectContext
        
        self.token = defaults.objectForKey("token") as? String
        if self.token == nil{
            self.performSegueWithIdentifier("login", sender: self)
        }else{
                self.title = defaults.objectForKey("nome") as? String
                self.fetchDisciplinas()
        }
    }
    
    @IBAction func logout(sender: AnyObject) {
        for disciplina in disciplinas{
            managedContext.deleteObject(disciplina)
        }
        
        var error : NSError?
        if !managedContext.saveToPersistentStore(&error){
            println("erro ao deletar registros \(error)")
        }
        NSUserDefaults.standardUserDefaults().removeObjectForKey("token")
        self.performSegueWithIdentifier("login", sender: self)
    }
    
    //MARK: RestKit/Core Data
    
    func initMapping(){
        
        var manager = RKObjectManager.sharedManager()
        
        let disciplinaResponseDescriptor = RKResponseDescriptor(
            mapping: ObjectMapping().disciplinaMapping(),
            method: RKRequestMethod.Any,
            pathPattern: nil,
            keyPath: "data",
            statusCodes: RKStatusCodeIndexSetForClass(RKStatusCodeClass.Successful))
        
        var errorResponseDescriptor = RKResponseDescriptor(
            mapping: ObjectMapping().errorMapping(),
            method: RKRequestMethod.Any,
            pathPattern: nil,
            keyPath: nil,
            statusCodes: RKStatusCodeIndexSetForClass(RKStatusCodeClass.ClientError))
        
        manager.addResponseDescriptor(disciplinaResponseDescriptor)
        manager.addResponseDescriptor(errorResponseDescriptor)
        
    }
    
    func carregarDisciplinas(){
        
        var manager = RKObjectManager.sharedManager()
        
        var parameters : NSDictionary = ["token":self.token!]
        
        manager.getObjectsAtPath("disciplinas/cursando", parameters: parameters as [NSObject : AnyObject], success:
            { (operation: RKObjectRequestOperation!, result:RKMappingResult!) -> Void in
                
                
                self.disciplinas.removeAll(keepCapacity: false)
                self.disciplinas = result.array() as! [Disciplina]
                
                self.tableView.reloadData()

                
            }, failure: { (operation:RKObjectRequestOperation!, error:NSError!) -> Void in
                
                var alert = UIAlertView()
                alert.message = operation.HTTPRequestOperation.responseString
                alert.addButtonWithTitle("Fechar")
                alert.show()
                
        })
        
    }
    
    
   
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
   
        return self.disciplinas.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: Cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! Cell
      
            let disciplina = self.disciplinas[indexPath.row]
            cell.disciplinaLabel.text = disciplina.nome
            cell.professorLabel.text = disciplina.professor.nome
            cell.turmaLabel.text = disciplina.turma.stringValue
        
        return cell
    }
    
    func fetchDisciplinas(){
        
        var fetchDisciplinas = NSFetchRequest(entityName: "Disciplina")
        var error: NSError?
        var results = self.managedContext.executeFetchRequest(fetchDisciplinas, error: &error) as? [Disciplina]
        
        if let r = results {
            
            if r.isEmpty {
                self.carregarDisciplinas()
            }else {
                self.disciplinas = r
            }
            
            
        }
        
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "detalhe" {
            var index = self.tableView.indexPathForSelectedRow()
            var disciplina = self.disciplinas[index!.row]
            var controller = segue.destinationViewController as! FeedDetailViewController
            
            controller.disciplina = disciplina
        }
        
    }
    
}
