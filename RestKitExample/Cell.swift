//
//  Ceel.swift
//  RestKitExample
//
//  Created by Gibran Tavares on 23/07/15.
//  Copyright (c) 2015 Gibran Tavares. All rights reserved.
//

import UIKit

class Cell: UITableViewCell {

    @IBOutlet weak var disciplinaLabel: UILabel!
    @IBOutlet weak var professorLabel: UILabel!
    @IBOutlet weak var turmaLabel: UILabel!
    
    
}
