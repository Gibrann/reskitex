//
//  Professor.swift
//  
//
//  Created by Gibran Tavares on 28/07/15.
//
//

import Foundation
import CoreData

@objc(Professor)
class Professor: NSManagedObject {

    @NSManaged var codigoProfessor: NSNumber
    @NSManaged var estabelecimento: String
    @NSManaged var matricula: String
    @NSManaged var nome: String

}
