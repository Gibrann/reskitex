//
//  FeedDetailViewController.swift
//  RestKitExample
//
//  Created by Gibran Tavares on 28/07/15.
//  Copyright (c) 2015 Gibran Tavares. All rights reserved.
//

import UIKit

class FeedDetailViewController: UIViewController {

    var disciplina: Disciplina!
    @IBOutlet weak var codigoLabel: UILabel!
    @IBOutlet weak var discLabel: UILabel!
    @IBOutlet weak var turmaLabel: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        codigoLabel.text = disciplina.codigo
        discLabel.text = disciplina.nome
        turmaLabel.text = disciplina.turma.stringValue
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
