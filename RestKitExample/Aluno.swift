//
//  Aluno.swift
//  RestKitExample
//
//  Created by Gibran Tavares on 22/07/15.
//  Copyright (c) 2015 Gibran Tavares. All rights reserved.
//

import UIKit

class Aluno: NSObject {

    var matricula: String!
    var senha: String!
    var origem: String!
    var token: String!
    var nome: String!
    var deviceId: String!
    var estabelecimento: String!
    var tipo: String!
    var dataAcesso: String!
    var dataUltimoAcesso: String!
    var status: String!
}
