//
//  Curso.swift
//  
//
//  Created by Gibran Tavares on 28/07/15.
//
//

import Foundation
import CoreData

@objc(Curso)
class Curso: NSManagedObject {

    @NSManaged var codigo: NSNumber
    @NSManaged var nome: String

}
