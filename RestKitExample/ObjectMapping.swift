//
//  ObjectMapping.swift
//  RestKitExample
//
//  Created by Gibran Tavares on 28/07/15.
//  Copyright (c) 2015 Gibran Tavares. All rights reserved.
//

import UIKit

class ObjectMapping: NSObject {
   
    let delegate: AppDelegate!
    
    override init(){
        self.delegate = UIApplication.sharedApplication().delegate as! AppDelegate
    }
    
    func cursoMapping() -> RKEntityMapping {
        
        var cursoMap = RKEntityMapping(forEntityForName: "Curso", inManagedObjectStore: self.delegate.manager.managedObjectStore)
        cursoMap.addAttributeMappingsFromArray(["codigo","nome"])
        
        cursoMap.identificationAttributes = ["codigo"]
        
        return cursoMap
    }
    
    func professorMapping() -> RKEntityMapping {
        
        var profMap = RKEntityMapping(forEntityForName: "Professor", inManagedObjectStore: self.delegate.manager.managedObjectStore)
        profMap.addAttributeMappingsFromArray(["estabelecimento","matricula","codigoProfessor","nome"])
        
        profMap.identificationAttributes = ["matricula"]
        
        return profMap
        
    }
    
    func disciplinaMapping() -> RKEntityMapping {
        
        var discMap = RKEntityMapping(forEntityForName: "Disciplina", inManagedObjectStore: self.delegate.manager.managedObjectStore)
        discMap.addAttributeMappingsFromArray(["identificador","codigo","nome","turma","creditoTeorico",
            "creditoPratico","horario","np1","np2","nf","faltas","frequencia","sala",
            "multiplosProfessores","ead"])
        
        discMap.identificationAttributes = ["identificador", "codigo"]
        
        discMap.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "professor", toKeyPath: "professor", withMapping: self.professorMapping()))
        discMap.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "curso", toKeyPath: "curso", withMapping: self.cursoMapping()))
        
        return discMap
    }
    
    func errorMapping() -> RKObjectMapping{
        var errorMapping = RKObjectMapping(forClass: RKErrorMessage.self)
        errorMapping.addPropertyMapping(RKAttributeMapping(fromKeyPath: nil, toKeyPath: "userInfo"))
        return errorMapping
    }
    
}
