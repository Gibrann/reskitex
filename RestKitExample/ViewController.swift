//
//  ViewController.swift
//  RestKitExample
//
//  Created by Gibran Tavares on 20/07/15.
//  Copyright (c) 2015 Gibran Tavares. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    var managedContext: NSManagedObjectContext!
    var token: String!
    let loading = UIAlertView()
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Entrar"
        self.emailField.text = "1320714"
        self.passwordField.text = "11111111"
    }

    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
    }

    @IBAction func request(sender: AnyObject) {
        let aluno = Aluno()
        aluno.matricula = self.emailField.text
        aluno.senha = self.passwordField.text
        aluno.origem = "ios"
        aluno.deviceId = nil
        
        var alerta = UIAlertView()
        alerta.title = "Erro"
        alerta.addButtonWithTitle("OK")
        
        if aluno.matricula == ""{
            alerta.message = "Matricula é obrigatória"
            alerta.show()
            return
        }
        
        if  aluno.senha == "" {
            alerta.message = "Password obrigatório"
            alerta.show()
            return

        }
        
        getLoginReq(aluno)
        
    }
    
       
    func getLoginReq(aluno: Aluno){
       
        let baseURL = "http://services-dev.unifor.br/services-dev/"
        
        var alunoResponseMapping = RKObjectMapping(forClass: Aluno.self)
        alunoResponseMapping.addAttributeMappingsFromArray(["token","nome"])
//        alunoResponseMapping.addAttributeMappingsFromArray([
//            "matricula","estabelecimento","tipo","nome",
//            "token","deviceId","dataAcesso","origem",
//            "dataUltimoAcesso","senha","status"])
        
        var alunoRequestMapping = RKObjectMapping.requestMapping()
        alunoRequestMapping.addAttributeMappingsFromArray(["matricula","senha","origem","deviceId"])
        
        var alunoResponseDescriptor = RKResponseDescriptor(
            mapping: alunoResponseMapping,
            method: RKRequestMethod.Any,
            pathPattern: nil,
            keyPath: "data",
            statusCodes: RKStatusCodeIndexSetForClass(RKStatusCodeClass.Successful))
        
        var alunoRequestDescriptor = RKRequestDescriptor(
            mapping: alunoRequestMapping,
            objectClass: Aluno.self,
            rootKeyPath: nil,
            method: RKRequestMethod.Any)
        
        
        let manager = RKObjectManager(baseURL: NSURL(string: baseURL)!)
        manager.addResponseDescriptor(alunoResponseDescriptor)
        manager.addRequestDescriptor(alunoRequestDescriptor)
        
        manager.postObject(aluno, path: "acesso/login/", parameters: nil,
            success: { (operation: RKObjectRequestOperation!, result:RKMappingResult!) -> Void in
                
                var alunoLogado = result.firstObject as! Aluno
                
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(alunoLogado.token!, forKey: "token")
                defaults.setObject(alunoLogado.nome!, forKey: "nome")
                self.token = alunoLogado.token!
                print("Aluno Logado! (token : \(alunoLogado.token!))")
                self.performSegueWithIdentifier("feedView", sender: self)
                
            }) { (operation: RKObjectRequestOperation!, error:NSError!) -> Void in
                
                var alert = UIAlertView()
                alert.message = "Erro"
                alert.addButtonWithTitle("Fechar")
                //alert.show()
                self.performSegueWithIdentifier("feedView", sender: self)

                
                
        }
        
        
    }
    
    
}

